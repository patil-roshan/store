import { View, Text } from 'react-native'
import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import Splash from './Screens/Splash';
import ProductDetail from './Screens/ProductDetail';
import Cart from './Screens/Cart';
import Signup from './Screens/Signup';
import Login from './Screens/Login';
import DrawerNavigation from './Screens/DrawerNavigation';
import Checkout from './Screens/Checkout';
import Addresses from './Screens/Addresses';
import AddAddress from './Screens/AddAddress';


const AppNavigator = () => {
    const Stack = createNativeStackNavigator();
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{ headerShown: false }}>
                <Stack.Screen name="Splash" component={Splash} />
                <Stack.Screen name="Main" component={DrawerNavigation} />
                <Stack.Screen name="ProductDetail" component={ProductDetail} />
                <Stack.Screen name="Cart" component={Cart} />
                <Stack.Screen name="Signup" component={Signup} />
                <Stack.Screen name="Login" component={Login} />
                <Stack.Screen name="Checkout" component={Checkout} />
                <Stack.Screen name="Addresses" component={Addresses} />
                <Stack.Screen name="AddAddress" component={AddAddress} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default AppNavigator