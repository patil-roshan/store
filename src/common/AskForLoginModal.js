import { Dimensions, Image, Modal, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import CustomButton from './CustomButton'

const AskForLoginModal = ({ modalVisible, onClickLogin, onClickSignUp, onClose }) => {
    return (
        <Modal visible={modalVisible} transparent>
            <View style={styles.modalView}>
                <View style={styles.mainView}>

                    {/* close buton */}
                    <View style={{ position: "absolute", top: 10, right: 10 }}>
                        <TouchableOpacity
                            onPress={() => { onClose() }}>
                            <Image source={require('../images/close.png')} style={{ width: 20, height: 20 }} />
                        </TouchableOpacity>
                    </View>

                    {/* login and signup button */}
                    <CustomButton
                        title={"Create an account"}
                        onClick={() => {
                            onClickSignUp()
                        }}
                        txtcolor={'#fff'}
                        bg={'#0087da'}
                        width={"90%"}
                    />
                    <CustomButton
                        title={"Log In"}
                        onClick={() => {
                            onClickLogin()
                        }}
                        txtcolor={'#fff'}
                        width={"90%"}
                        bg={'#0087da'}
                    />

                </View>
            </View>
        </Modal>
    )
}

export default AskForLoginModal

const styles = StyleSheet.create({
    modalView: {
        flex: 1,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: 'rgba(0, 0, 0, 0.9)',
        justifyContent: "center",
        alignItems: "center",
    },
    mainView: {
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#fff",
        height: Dimensions.get('window').height / 4,
        width: Dimensions.get('window').width - 25,
        borderRadius: 10
    }
})