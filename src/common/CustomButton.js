import { Dimensions, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'

const CustomButton = ({ bg, title, onClick, icon, txtcolor, width }) => {
    return (
        <TouchableOpacity
            activeOpacity={0.9}
            onPress={() => { onClick() }}
            style={[styles.btn, { backgroundColor: bg, width: width }]}
        >
            <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={[styles.btnText, { color: txtcolor }]}>{title}</Text>
                {icon &&
                    <Image source={icon} style={[styles.icon, { tintColor: txtcolor }]} />

                }
            </View>

        </TouchableOpacity>
    )
}

export default CustomButton

const styles = StyleSheet.create({
    btn: {
        height: 50,
        justifyContent: "center",
        alignSelf: "center",
        alignItems: "center",
        borderRadius: 10,
        marginVertical: 10,
        padding: 10,
    },
    btnText: {
        fontSize: 20,
        fontWeight: "500",
    },
    icon: {
        width: 30,
        height: 30,
        marginLeft: 10,
    }
})