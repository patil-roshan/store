import { useNavigation } from '@react-navigation/native';
import { Dimensions, Image, StyleSheet, Text, TouchableNativeFeedback, TouchableOpacity, View } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { useSelector } from 'react-redux';

const { height, width } = Dimensions.get('window');

const Header = ({
    title,
    leftIcon,
    rightIcon,
    onClickLeftIcon,
    onClickRightIcon,
    isCart = false,
}) => {
    const cartItems = useSelector(state => state.cart)
    const navigation = useNavigation();
    return (
        <View style={styles.header}>
            <TouchableOpacity
                background={TouchableNativeFeedback.Ripple('#06bcee', false)}
                activeOpacity={0.8}
                style={styles.btn}
                onPress={() => {
                    onClickLeftIcon();
                }}
            >
                <Image source={leftIcon} style={styles.icon} onPress={onClickLeftIcon} />
            </TouchableOpacity>

            <Text style={styles.title}>{title}</Text>

            {isCart && <View></View>}

            {!isCart && (
                <TouchableOpacity
                    background={TouchableNativeFeedback.Ripple('#06bcee', false)}
                    activeOpacity={0.8}
                    style={styles.btn}
                    onPress={() => {
                        navigation.navigate("Cart")
                    }}
                >
                    <Image
                        source={rightIcon}
                        style={styles.icon}
                        onPress={onClickLeftIcon}
                    />
                    <View style={{ flex: 1, width: 20, alignItems: "center", justifyContent: "center", height: 20, backgroundColor: "#FFF", borderRadius: 10, position: "absolute", right: 0, top: 10 }}>
                        <Text style={{ color: "#000" }}>{cartItems.data.length}</Text>
                    </View>
                </TouchableOpacity>
            )}
        </View >
    )
}

export default Header

const styles = StyleSheet.create({
    header: {
        height: hp("7%"),
        backgroundColor: "#0786DAFD",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingLeft: 15,
        paddingRight: 15,
    },
    btn: {
        width: 40,
        height: 40,
        justifyContent: "center",
        alignItems: "center"
    },
    icon: {
        width: 30,
        height: 30,
        tintColor: "#ffffff"
    },
    title: {
        color: "#ffffff",
        fontSize: 25,
    }
})