import { Dimensions, FlatList, Image, StyleSheet, Text, View } from 'react-native'
import React from 'react'


const Carousal = ({ data }) => {
    const carousalData = data
    // console.log("carousalData:", carousalData);
    return (

        <View style={{ backgroundColor: "#fff", elevation: 5, }}>
            <FlatList
                horizontal={true}
                data={carousalData}
                pagingEnabled={true}
                snapToAlignment='center'
                showsHorizontalScrollIndicator={false}
                renderItem={({ item, index }) => (
                    <View key={index} >
                        <Image source={{ uri: item }} style={styles.img} />
                    </View>
                )}
            />
        </View>
    )
}

export default Carousal

const styles = StyleSheet.create({
    img: {
        width: Dimensions.get("window").width,
        height: Dimensions.get("window").height / 2.45,
        resizeMode: "contain",
        marginVertical: 30,
    }
})