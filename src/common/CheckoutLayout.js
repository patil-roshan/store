import { Dimensions, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import CustomButton from './CustomButton'
import { useNavigation } from '@react-navigation/native'

const CheckoutLayout = ({ total, items }) => {
const navigation = useNavigation();
    return (
        <View style={styles.container}>
            <View style={styles.tab}>
                <Text>{`(Items ${items})`}</Text>
                <Text style={{ fontWeight: "700", fontSize: 20 }}>{'total: $' + total}</Text>
            </View>
            <View style={styles.tab}>
                <CustomButton
                    title={"Checkout"}
                    onClick={() => {
                        navigation.navigate("Checkout")
                    }}
                    txtcolor={'#fff'}
                    bg={'#007aff'}
                    width={"75%"}
                />
            </View>
        </View>
    )
}

export default CheckoutLayout

const styles = StyleSheet.create({
    container: {
        position: "absolute",
        bottom: 0,
        height: 70,
        width: Dimensions.get('window').width,
        backgroundColor: "#fff",
        flexDirection: "row",
    },
    tab: {
        width: '50%',
        height: '100%',
        justifyContent: "center",
        alignItems: "center"
    }
})