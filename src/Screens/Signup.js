import { Dimensions, StyleSheet, Text, TextInput, ToastAndroid, TouchableOpacity, View } from 'react-native'
import React, { useState } from 'react'
import LottieView from 'lottie-react-native'
import CustomButton from '../common/CustomButton'
import { useNavigation } from '@react-navigation/native'
import firestore from '@react-native-firebase/firestore';


const Signup = () => {
    const navigation = useNavigation();
    const [name, setName] = useState('');
    const [mobile, setMobile] = useState('');
    const [email, setEmail] = useState('');
    const [pass, setPass] = useState('');
    const [confirmPass, seConfirmPassword] = useState('');

    const addUser = () => {
        firestore()
            .collection('Users')
            .add({
                name: name,
                email: email,
                password: pass,
                mobile: mobile,
            })
            .then(() => {
                console.log('User added!');
                ToastAndroid.show('Signup successfull', ToastAndroid.SHORT);
                navigation.navigate('Login');
            });
    }
    return (
        <View style={styles.container}>

            <LottieView
                style={styles.icon}
                source={require('../images/login.json')}
                autoPlay
                loop={false}
            />
            <Text style={styles.title}>Create account</Text>
            <TextInput
                placeholder='Enter your name'
                placeholderTextColor={"#afafaf"}
                value={name}
                onChangeText={txt => setName(txt)}
                style={styles.input}
            />
            <TextInput
                placeholder='Enter email'
                placeholderTextColor={"#afafaf"}
                value={email}
                onChangeText={txt => setEmail(txt)}
                style={styles.input}
            />
            <TextInput
                placeholder='Enter mobile'
                placeholderTextColor={"#afafaf"}
                value={mobile}
                onChangeText={txt => setMobile(txt)}
                style={styles.input}
            />
            <TextInput
                placeholder='Enter password'
                placeholderTextColor={"#afafaf"}
                value={pass}
                onChangeText={txt => setPass(txt)}
                style={styles.input}
            />
            <TextInput
                placeholder='Enter confirm password'
                placeholderTextColor={"#afafaf"}
                value={confirmPass}
                onChangeText={txt => seConfirmPassword(txt)}
                style={styles.input}
            />
            <CustomButton
                title={"Sign Up"}
                onClick={() => {
                    addUser();
                }}
                txtcolor={'#fff'}
                bg={'#0087da'}
                width={Dimensions.get('window').width - 50}

            />

            <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                <Text style={styles.login}>Already have an account?{" "}
                    <Text style={[styles.login, { color: '#0087da', textDecorationLine: 'underline' }]}>Login</Text>
                </Text>
            </TouchableOpacity> 

        </View>
    )
}

export default Signup

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    icon: {
        height: Dimensions.get('window').height / 10,
        width: Dimensions.get('window').width
    },
    title: {
        textAlign: 'center',
        color: '#000',
        fontSize: 30,
        marginVertical: 10
    },
    input: {
        width: Dimensions.get('window').width - 50,
        height: 50,
        borderWidth: 1,
        borderRadius: 10,
        marginVertical: 10,
        paddingHorizontal: 10
    },
    login: {
        color: '#000',
        fontSize: 18,
        marginTop: 10
    }

})