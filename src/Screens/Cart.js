import { Dimensions, FlatList, StyleSheet, Text, View, TouchableOpacity, Image, ToastAndroid } from 'react-native'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Header from '../common/Header';
import { useNavigation } from '@react-navigation/native';
import { addItemToCart, reduceItemFromCart, removeItemFromCart } from '../redux/slices/CartSlice';
import CustomButton from '../common/CustomButton';
import LottieView from 'lottie-react-native';
import CheckoutLayout from '../common/CheckoutLayout';


const Cart = () => {
    const items = useSelector(state => state.cart);
    const navigation = useNavigation();
    const [cartItems, setCartItems] = useState([]);
    const dispatch = useDispatch();
    useEffect(() => {
        setCartItems(items.data);
    }, [items])

    const getTotal = () => {
        let total = 0;
        cartItems.map(item => {
            total = total + item.qty * item.price
        })
        return total.toFixed(2);
    }


    console.log(cartItems);
    return (
        <View style={styles.container}>
            <Header
                leftIcon={require('../images/back.png')}
                title={"Cart"}
                rightIcon={require('../images/cart.png')}
                onClickLeftIcon={
                    () => {
                        navigation.goBack()
                    }
                }
                isCart={true}
            />
            {cartItems.length === 0 ?
                <View style={{ flex: 1, backgroundColor: "#f8f8f8", alignItems: "center", justifyContent: "center" }}>

                    <LottieView
                        style={{
                            height: Dimensions.get('window').height / 4,
                            width: Dimensions.get('window').width,
                            // marginTop: "-30%",
                        }}
                        source={require('../images/cart.json')}
                        autoPlay
                        loop
                    />
                    <Text style={{ color: "#000", fontSize: 20, }}> your cart is empty   </Text>
                    <Text style={{ color: "#000", fontSize: 14, textAlign: "center", }}>
                        Looks like you have not added anything to you cart. {"\n"}
                        Go ahead & explore top categories,</Text>
                    <CustomButton
                        bg={"#0087da"}
                        title={"Return to shop"}
                        onClick={() => {
                            navigation.replace("Main")
                        }}
                        txtcolor={"#fff"}
                    />
                </View> : (
                    <FlatList
                        data={cartItems}
                        renderItem={({ item, index }) => {
                            return (
                                <TouchableOpacity
                                    activeOpacity={0.85}
                                    style={styles.productItem}
                                    onPress={
                                        () => {
                                            navigation.navigate("ProductDetail", { data: item })
                                            console.log("item:", item)
                                        }
                                    }
                                >
                                    <Image
                                        source={{ uri: item.thumbnail }}
                                        style={styles.itemImage} />
                                    <View>
                                        <Text style={styles.name}>
                                            {item.title.length > 25
                                                ? item.title.substring(0, 25) + '..'
                                                : item.title}
                                            {/* {item.title} */}
                                        </Text>
                                        <Text style={styles.desc}>
                                            {item.description.length > 40
                                                ? item.description.substring(0, 40) + '...'
                                                : item.description}
                                        </Text>
                                        <TouchableOpacity
                                            activeOpacity={0.85}
                                            onPress={() => { navigation.navigate("ProductDetail", { data: item }) }}
                                        >
                                        </TouchableOpacity>
                                        <View style={styles.qntView}>

                                            <Text style={styles.price}>{'$ ' + item.price}</Text>
                                            <TouchableOpacity
                                                style={{ paddingLeft: 10, textAlign: "center" }}
                                                onPress={() => {
                                                    if (item.qty > 1) {
                                                        dispatch(reduceItemFromCart(item))
                                                    } else {
                                                        dispatch(removeItemFromCart(index))
                                                    }
                                                }}
                                            >
                                                <Text style={styles.btn}> - </Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={{ paddingLeft: 10, textAlign: "center" }}>
                                                <Text style={styles.btnQnt}> {item.qty} </Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity
                                                style={{ paddingLeft: 10, textAlign: "center" }}
                                                onPress={() => {
                                                    dispatch(addItemToCart(item))
                                                }}>
                                                <Text style={styles.btn}> + </Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>

                                </TouchableOpacity>
                            )
                        }}
                    />)
            }

            {cartItems.length > 0 && (
                <CheckoutLayout items={cartItems.length} total={getTotal()} />
            )}
        </View >
    )
}

export default Cart

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    text: {
        color: "#000",
    },
    productItem: {
        width: Dimensions.get('window').width,
        height: 100,
        marginVertical: 5,
        backgroundColor: '#FFF',
        alignItems: "center",
        flexDirection: "row",
    },
    itemImage: {
        width: 100,
        height: 100,
        objectFit: "contain",
        marginLeft: 10,
    },
    name: {
        fontSize: 18,
        fontWeight: "600",
        marginLeft: 20,
        color: "#313232",
    },
    desc: {
        marginLeft: 20,
        color: "#3e3e3e",
    },
    link: {
        textDecorationLine: "underline",
        color: "#0087da",
        marginLeft: 20,
    },
    price: {
        color: "green",
        fontSize: 18,
        fontWeight: "600",
        marginLeft: 20,
    },
    qntView: {
        flexDirection: "row",
        paddingTop: 10,
    },
    btn: {
        width: 45,
        fontSize: 18,
        borderWidth: 0.2,
        borderRadius: 100,
        color: "#000",
        textAlign: "center",
    },
    btnQnt: {
        color: "#000",
        fontSize: 18,
    }
})