import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Home from './tabs/Home';
import Search from './tabs/Search';
import Wishlist from './tabs/Wishlist';
import Notification from './tabs/Notification';
import User from './tabs/User';
import { Image } from 'react-native';

const Tab = createBottomTabNavigator();

const HomeScreen = () => {
    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;

                    if (route.name === 'Home') {
                        iconName = focused ? require('../images/home_fill.png') : require('../images/home.png');
                    } else if (route.name === 'Search') {
                        iconName = focused ? require('../images/search_fill.png') : require('../images/search.png');
                    } else if (route.name === 'Wishlist') {
                        iconName = focused ? require('../images/wishlist_fill.png') : require('../images/wishlist.png');
                    } else if (route.name === 'Notification') {
                        iconName = focused ? require('../images/notification_fill.png') : require('../images/notification.png');
                    } else if (route.name === 'User') {
                        iconName = focused ? require('../images/user_fill.png') : require('../images/user.png');
                    }

                    return <Image source={iconName} style={{ width: size, height: size, tintColor: color }} />;
                },
            })}
        >
            <Tab.Screen name="Home" component={Home} options={{ headerShown: false }} />
            <Tab.Screen name="Search" component={Search} options={{ headerShown: false }} />
            <Tab.Screen name="Wishlist" component={Wishlist} options={{ headerShown: false }} />
            <Tab.Screen name="Notification" component={Notification} options={{ headerShown: false }} />
            <Tab.Screen name="User" component={User} options={{ headerShown: false }} />
        </Tab.Navigator>
    );
};

export default HomeScreen;