import { Dimensions, StyleSheet, Text, View } from 'react-native'
import React, { useEffect } from 'react'
import LottieView from 'lottie-react-native';

const Splash = ({ navigation }) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.replace('Main')
            // }, 4000)
        }, 1000)
    }, [])

    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>

            <LottieView
                style={{
                    height: Dimensions.get('window').height / 2,
                    width: Dimensions.get('window').width,
                    borderWidth: 5,
                    borderColor: "red"
                }}
                source={require('../images/splash.json')}
                autoPlay
                loop
            />
        </View>
    )
}

export default Splash

const styles = StyleSheet.create({})