import { StyleSheet, Text, View, TouchableOpacity, Image, TextInput } from 'react-native'
import React, { useState } from 'react'
import Header from '../common/Header'
import { useNavigation } from '@react-navigation/native'
import { coreModule } from '@reduxjs/toolkit/query'
import CustomButton from '../common/CustomButton'
import { useDispatch } from 'react-redux'
import { addAddress } from '../redux/slices/AddressSlice'

const AddAddress = () => {
    const navigation = useNavigation();
    const [type, setType] = useState(0);
    const [address1, setAddress1] = useState("");
    const [address2, setAddress2] = useState("");
    const [city, setCity] = useState("");
    const [state, setState] = useState("");
    const [zipCode, setZipCode] = useState("");
    const [country, setCountry] = useState("");

    const dispatch = useDispatch();
    return (
        <View style={styles.container}>
            <Header
                leftIcon={require('../images/back.png')}
                title={"Add new address"}
                rightIcon={require('../images/cart.png')}
                onClickLeftIcon={() => {
                    navigation.goBack()
                }}
                onClickRightIcon={() => {
                    navigation.navigate('Cart')
                }}
            />

            <TextInput
                style={[styles.input, { marginTop: 50 }]}
                placeholder='Address line 1'
                placeholderTextColor={"#afafaf"}
                value={address1}
                onChangeText={(text) => setAddress1(text)}
            />
            <TextInput
                style={styles.input}
                placeholder='Address line 2'
                placeholderTextColor={"#afafaf"}
                value={address2}
                onChangeText={(text) => setAddress2(text)}
            />
            <TextInput
                style={styles.input}
                placeholder='City'
                placeholderTextColor={"#afafaf"}
                value={city}
                onChangeText={(text) => setCity(text)}
            />
            <TextInput
                style={styles.input}
                placeholder='State / province'
                placeholderTextColor={"#afafaf"}
                value={state}
                onChangeText={(text) => setState(text)}
            />
            <TextInput
                style={styles.input}
                placeholder='Zip/ Postal code'
                keyboardType='number-pad'
                placeholderTextColor={"#afafaf"}
                value={zipCode}
                onChangeText={(text) => setZipCode(text)}
            />
            <TextInput
                style={styles.input}
                placeholder='Country'
                placeholderTextColor={"#afafaf"}
                value={country}
                onChangeText={(text) => setCountry(text)}
            />
            <View style={styles.typeView}>
                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() => { setType(0) }}
                    style={[
                        styles.typeBtn,
                        { borderWidth: 0.5, borderColor: type === 0 ? '#0087da' : "#fff" }
                    ]}>
                    <Image
                        source={type === 0
                            ? require('../images/radio_fill.png')
                            : require('../images/radio.png')
                        } style={styles.img} />
                    <Text style={styles.radioLabel}>Home</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() => { setType(1) }}
                    style={[
                        styles.typeBtn,
                        { borderWidth: 0.5, borderColor: type === 1 ? '#0087da' : "#fff" }
                    ]}>
                    <Image
                        source={type === 1
                            ? require('../images/radio_fill.png')
                            : require('../images/radio.png')
                        } style={styles.img} />
                    <Text style={styles.radioLabel}>Office</Text>
                </TouchableOpacity>
            </View>
            <CustomButton
                bg={"#0087da"}
                title={"Save address"}
                onClick={() => {
                    dispatch(addAddress({
                        address1: address1,
                        address2: address2,
                        city: city,
                        state: state,
                        zipCode: zipCode,
                        country: country,
                        type: type === 0 ? "home" : "office"
                    }),
                    navigation.goBack()
                    )
                }}
                txtcolor={"#fff"}
                width={"90%"}
            />

        </View>
    )
}

export default AddAddress

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
    },
    input: {
        width: "90%",
        height: 50,
        borderWidth: 0.25,
        borderRadius: 10,
        alignSelf: "center",
        paddingLeft: 20,
        marginVertical: 10,
    },
    typeView: {
        width: "100%",
        flexDirection: "row",
        marginVertical: 20,
        justifyContent: "space-evenly",
        alignItems: "center"
    },
    typeBtn: {
        width: "40%",
        height: 50,
        borderWidth: 0.5,
        borderRadius: 10,
        flexDirection: "row",
        paddingLeft: 10,
        alignItems: "center",
    },
    img: {
        width: 24,
        height: 24,
        tintColor: "#0087da"
    },
    radioLabel: {
        marginLeft: 10,
    }
})