import { StyleSheet, Text, View, TouchableOpacity, Image, FlatList } from 'react-native'
import React, { useEffect } from 'react'
import Header from '../common/Header'
import { useIsFocused, useNavigation } from '@react-navigation/native'
import { coreModule } from '@reduxjs/toolkit/query'
import { useSelector } from 'react-redux'

const Addresses = () => {
    const navigation = useNavigation();
    const addressList = useSelector(state => state.address);
    console.log(addressList.data);
    const isFocused = useIsFocused();
    useEffect(() => {
        console.log(addressList);
    }, [isFocused])
    return (
        <View style={styles.container}>
            <Header
                leftIcon={require('../images/back.png')}
                title={"Addresses"}
                rightIcon={require('../images/cart.png')}
                onClickLeftIcon={() => {
                    navigation.goBack()
                }}
                onClickRightIcon={() => {
                    navigation.navigate('Cart')
                }}
            />

            <FlatList
                data={addressList.data}
                renderItem={({ item, index }) => (
                    <View style={{ width: "90%", height: 100, backgroundColor: "fff", borderWidth: 0.5, alignSelf: "center" }}>
                        <Text style={{ fontWeight: "bold", fontSize: 18 }}>{item.type}</Text>
                        <Text>{item.address1} {item.address2}</Text>
                        <Text>{item.city}</Text>
                        <Text>{item.state}</Text>
                        <Text>{item.zipCode}</Text>
                    </View>
                )}
            />

            <TouchableOpacity
                style={styles.addButton}
                activeOpacity={0.7}
                onPress={() => navigation.navigate('AddAddress')}
            >
                <Image source={require('../images/plus.png')} style={styles.img} />
            </TouchableOpacity>
        </View>
    )
}

export default Addresses

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
    },
    addButton: {
        width: 50,
        height: 50,
        borderRadius: 50,
        backgroundColor: "#0087da",
        position: "absolute",
        bottom: 20,
        right: 20,
        alignItems: "center",
        justifyContent: "center"
    },
    img: {
        width: 24,
        height: 24,
        tintColor: "#fff"
    }
})