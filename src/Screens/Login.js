import { Dimensions, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import React, { useState } from 'react'
import LottieView from 'lottie-react-native'
import CustomButton from '../common/CustomButton'
import { useNavigation } from '@react-navigation/native'
import firestore from '@react-native-firebase/firestore';
import { weakMapMemoize } from '@reduxjs/toolkit'


const Login = () => {
    const navigation = useNavigation();
    const [email, setEmail] = useState('');
    const [pass, setPass] = useState('');

    const loginUser = () => {
        firestore()
            .collection('Users')
            // Filter results
            .where('email', '==', email)
            .get()
            .then(querySnapshot => {
                /* ... */
                console.log(querySnapshot.docs[0]._data);
            });
    }
    return (
        <View style={styles.container}>

            <LottieView
                style={styles.icon}
                source={require('../images/login.json')}
                autoPlay
                loop={false}
            />
            <Text style={styles.title}>Login</Text>
            <TextInput
                placeholderTextColor={"#afafaf"}
                placeholder='Enter email'
                style={styles.input}
                value={email}
                onChangeText={txt => setEmail(txt)}
            />
            <TextInput
                placeholderTextColor={"#afafaf"}
                placeholder='Enter password'
                style={styles.input}
                value={pass}
                onChangeText={txt => setPass(txt)}
            />
            <CustomButton
                title={"Login"}
                onClick={() => { loginUser() }}
                txtcolor={'#fff'}
                bg={'#0087da'}
                width={Dimensions.get('window').width - 50}

            />

            <TouchableOpacity onPress={() => navigation.navigate('Signup')}>
                <Text style={styles.login}>Create new account?{" "}
                    <Text style={[styles.login, { color: '#0087da', textDecorationLine: 'underline' }]}>Signup</Text>
                </Text>
            </TouchableOpacity>

        </View>
    )
}

export default Login

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    icon: {
        height: Dimensions.get('window').height / 10,
        width: Dimensions.get('window').width
    },
    title: {
        textAlign: 'center',
        color: '#000',
        fontSize: 30,
        marginVertical: 10
    },
    input: {
        width: Dimensions.get('window').width - 50,
        height: 50,
        borderWidth: 1,
        borderRadius: 10,
        marginVertical: 10,
        paddingHorizontal: 10
    },
    login: {
        color: '#000',
        fontSize: 18,
        marginTop: 10
    }

})