import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, ToastAndroid, View, Alert, Dimensions } from 'react-native'
import React, { useState } from 'react'
import Header from '../common/Header'
import { useNavigation, useRoute } from '@react-navigation/native'
import CustomButton from '../common/CustomButton'
import { useDispatch } from 'react-redux'
import { addItemToWishList } from '../redux/slices/WishlistSlice'
import { addItemToCart } from '../redux/slices/CartSlice'
import AsyncStorage from '@react-native-async-storage/async-storage'
import AskForLoginModal from '../common/AskForLoginModal'
import Carousal from '../common/Carousal'

const ProductDetail = () => {
    const navigation = useNavigation()
    const route = useRoute()
    const dispatch = useDispatch()
    const [qnty, setQnty] = useState(1);
    const [modalVisible, setModalVisible] = useState(false);

    const checkUserStatus = async () => {
        let isUserLoggedIn = false;
        const status = await AsyncStorage.getItem("IS_USER_LOGGED_IN");
        if (status === null) {
            isUserLoggedIn = false;
        } else {
            isUserLoggedIn = true;
        }
        return isUserLoggedIn;
    }

    console.log("product data:", route.params.data);
    return (
        <View style={styles.container}>
            <Header
                leftIcon={require('../images/back.png')}
                title={"Product Detail"}
                rightIcon={require('../images/cart.png')}
                onClickLeftIcon={() => {
                    navigation.goBack()
                }}
            />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View>
                    <View>
                        <Carousal data={route.params.data.images} style={styles.banner} />
                    </View>
                    {/* <Image source={{ uri: route.params.data.images[0] }} style={styles.banner} /> */}
                    <Text style={styles.title}>{route.params.data.title} </Text>
                    <Text style={styles.brand}>{route.params.data.brand} </Text>
                    <Text style={styles.desc}>{route.params.data.description} </Text>
                    <View style={styles.ratingContainer}>
                        <Text style={styles.rating}> {route.params.data.rating}</Text>
                        <Image source={require('../images/rating.png')} style={{ width: 20, height: 20, tintColor: "green", }} />
                    </View>
                    <Text style={styles.price}>
                        <Text style={[styles.price, { color: "#000" }]}>Price: </Text>
                        {"$" + route.params.data.price}
                    </Text>
                </View>
                <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center" }}>
                    <TouchableOpacity
                        onPress={() => {
                            if (qnty > 1) {
                                setQnty(qnty - 1)
                            }
                        }}
                    ><Text style={styles.qtyBtn}> - </Text></TouchableOpacity>

                    <Text style={{ color: "#000", fontSize: 25 }}> {qnty} </Text>

                    <TouchableOpacity
                        onPress={() => {
                            setQnty(qnty + 1)
                        }}
                    ><Text style={styles.qtyBtn}> + </Text></TouchableOpacity>
                </View>
                <View style={{ flexDirection: "row", justifyContent: "space-around", alignItems: "center" }}>
                    <CustomButton
                        title={"Add to cart"}
                        bg={"#0087da"}
                        txtcolor={"#fff"}
                        icon={require('../images/cart.png')}
                        onClick={() => {
                            // if (!checkUserStatus()) {
                            dispatch(addItemToCart({
                                category: route.params.data.category,
                                description: route.params.data.description,
                                id: route.params.data.id,
                                images: route.params.data.images,
                                price: route.params.data.price,
                                qty: qnty,
                                rating: route.params.data.rating,
                                title: route.params.data.title,
                                thumbnail: route.params.data.thumbnail
                            })
                            );
                            ToastAndroid.show("Item added to cart", ToastAndroid.SHORT)
                            // }
                            // else {
                            //     // Alert.alert("Error", "Please login to add item to cart")
                            //     setModalVisible(true)
                            // }
                        }}
                    />

                    <CustomButton
                        title={"Add to wishlist"}
                        bg={"#0087da"}
                        txtcolor={"#fff"}
                        icon={require('../images/wishlist.png')}
                        onClick={() => {
                            if (!checkUserStatus()) {
                                dispatch(addItemToWishList(route.params.data))
                                ToastAndroid.show("Item added to wishlist", ToastAndroid.SHORT)
                            } else {
                                // Alert.alert("Error", "Please login to add item to wishlist")
                                setModalVisible(true)
                            }
                        }}

                    />

                </View>
            </ScrollView >
            <AskForLoginModal
                modalVisible={modalVisible}
                onClickLogin={() => {
                    navigation.navigate("Login")
                    setModalVisible(false)
                }}
                onClose={() => {
                    setModalVisible(false)
                }}
                onClickSignUp={() => {
                    navigation.navigate("Signup")
                    setModalVisible(false)
                }}


            />
        </View >
    )
}

export default ProductDetail

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff"
    },
    banner: {
        marginTop: 20,
        height: Dimensions.get('window').height / 2.05,
        resizeMode: "contain",
        marginHorizontal: 10
    },
    title: {
        fontSize: 30,
        fontWeight: "700",
        marginHorizontal: 10,
        marginTop: 20,
        color: "#000"
    },
    brand: {
        color: "green",
        marginHorizontal: 10,
        fontSize: 20,
        fontWeight: "700"
    },
    ratingContainer: {
        backgroundColor: "#fff",
        borderRadius: 25,
        marginTop: 20,
        width: Dimensions.get('window').width / 3.5,
        flex: 0.1,
        marginHorizontal: 10,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        elevation: 5
    },
    rating: {
        fontSize: 20,
        fontWeight: "700",
        // marginHorizontal: 10,
        color: "#000",
        padding: 10,
    },
    desc: {
        color: "#000",
        marginTop: 10,
        marginHorizontal: 10,
        fontSize: 20
    },
    price: {
        fontSize: 30,
        fontWeight: "700",
        marginTop: 20,
        marginHorizontal: 10,
        color: "green"
    },
    qtyBtn: {
        color: "#000",
        width: 50,
        textAlign: "center",
        borderWidth: 0.5,
        borderRadius: 50,
        padding: 10,
        marginHorizontal: 5,
        fontSize: 20
    }

})