import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { createDrawerNavigator } from '@react-navigation/drawer';
import HomeScreen from './HomeScreen';
import Cart from './Cart';

const Drawer = createDrawerNavigator();

const DrawerNavigation = () => {
    return (
        <>
            <Drawer.Navigator screenOptions={{ headerShown: false }} >
                <Drawer.Screen name="Homescreen" component={HomeScreen} />
                <Drawer.Screen name="Cart" component={Cart} />
            </Drawer.Navigator>
        </>
    )
}

export default DrawerNavigation

const styles = StyleSheet.create({})