import { View, Text, StyleSheet, TouchableOpacity, Image, SafeAreaView, Keyboard } from 'react-native'
import React, { useEffect, useState } from 'react'
import Home from './tabs/Home'
import Search from './tabs/Search'
import Wishlist from './tabs/Wishlist'
import Notification from './tabs/Notification'
import User from './tabs/User'

const HomeScreen = () => {
    const [selectedTab, setSelectedTab] = useState(0)
    const [isKeyboadVisible, setIsKeyboadVisible] = useState(false)

    useEffect(() => {
        const keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            () => {
                setIsKeyboadVisible(true); // or some other action
            }
        );
        const keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            () => {
                setIsKeyboadVisible(false); // or some other action
            }
        );

        return () => {
            keyboardDidHideListener.remove();
            keyboardDidShowListener.remove();
        };
    }, []);

    return (
        <View style={styles.container}>


            {selectedTab == 0 ? (
                <Home />
            ) : selectedTab == 1 ? (
                <Search />
            ) : selectedTab == 2 ? (
                <Wishlist />
            ) : selectedTab == 3 ? (
                <Notification />
            ) : selectedTab == 4 ? (
                <User />
            ) : ""
            }
            {!isKeyboadVisible && (
                <View style={styles.bottomView}>
                    <TouchableOpacity
                        onPress={() => {
                            setSelectedTab(0)
                        }}
                        style={styles.bottomTab}>
                        <Image source={
                            selectedTab == 0 ?
                                require('../images/home_fill.png') :
                                require('../images/home.png')
                        } style={styles.bottomTabIcon} />
                    </TouchableOpacity>


                    <TouchableOpacity
                        onPress={() => {
                            setSelectedTab(1)
                        }}
                        style={styles.bottomTab}>
                        <Image source={
                            selectedTab == 1 ?
                                require('../images/search_fill.png') :
                                require('../images/search.png')
                        } style={styles.bottomTabIcon} />
                    </TouchableOpacity>


                    <TouchableOpacity
                        onPress={() => {
                            setSelectedTab(2)
                        }}
                        style={styles.bottomTab}>
                        <Image source={
                            selectedTab == 2 ?
                                require('../images/wishlist_fill.png') :
                                require('../images/wishlist.png')
                        } style={styles.bottomTabIcon} />
                    </TouchableOpacity>


                    <TouchableOpacity
                        onPress={() => {
                            setSelectedTab(3)
                        }}
                        style={styles.bottomTab}>
                        <Image source={
                            selectedTab == 3 ?
                                require('../images/notification_fill.png') :
                                require('../images/notification.png')
                        } style={styles.bottomTabIcon} />
                    </TouchableOpacity>


                    <TouchableOpacity
                        onPress={() => {
                            setSelectedTab(4)
                        }}
                        style={styles.bottomTab}>
                        <Image source={
                            selectedTab == 4 ?
                                require('../images/user_fill.png') :
                                require('../images/user.png')
                        } style={styles.bottomTabIcon} />
                    </TouchableOpacity>
                </View>
            )}

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    bottomView: {
        width: "100%",
        height: 70,
        flexDirection: "row",
        justifyContent: "space-evenly",
        backgroundColor: "#FFFFFF",
        borderTopWidth: 0.125,
        elevation: 10,

    },
    bottomTab: {
        width: "20%",
        height: "100%",
        justifyContent: "center",
        alignItems: "center"
    },
    bottomTabIcon: {
        width: 24,
        height: 24,
        tintColor: "#000000"
    }
})

export default HomeScreen