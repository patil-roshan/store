import { Dimensions, FlatList, Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import Header from '../common/Header'
import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import CustomButton from '../common/CustomButton';

const Checkout = () => {
    const navigation = useNavigation();
    const items = useSelector(state => state.cart);
    const [cartItems, setCartItems] = useState([]);
    const [selectedMethod, setselectedMethod] = useState(0);
    const [selectedAddress, setSelectedAddress] = useState("Please select an address");
    const dispatch = useDispatch();
    useEffect(() => {
        setCartItems(items.data);
    }, [items])

    const getTotal = () => {
        let total = 0;
        cartItems.map(item => {
            total = total + item.qty * item.price
        })
        return total.toFixed(2);
    }
    return (
        <ScrollView style={styles.container}>
            <View >
                <Header
                    leftIcon={require('../images/back.png')}
                    title={"Checkout"}
                    onClickLeftIcon={() => {
                        navigation.goBack()
                    }}
                    isCart={true}
                />
                <Text style={styles.title}>Added Items</Text>
                <View>
                    <FlatList
                        data={cartItems}
                        renderItem={({ item, index }) => {
                            return (
                                <TouchableOpacity
                                    activeOpacity={0.85}
                                    style={styles.productItem}
                                    onPress={
                                        () => {
                                            navigation.navigate("ProductDetail", { data: item })
                                            // console.log("item:", item.id)
                                        }
                                    }
                                >
                                    <Image
                                        source={{ uri: item.image }}
                                        style={styles.itemImage} />
                                    <View>
                                        <Text style={styles.name}>
                                            {item.title.length > 25
                                                ? item.title.substring(0, 25) + '..'
                                                : item.title}
                                            {/* {item.title} */}
                                        </Text>
                                        <Text style={styles.desc}>
                                            {item.description.length > 40
                                                ? item.description.substring(0, 40) + '...'
                                                : item.description}
                                        </Text>
                                        <TouchableOpacity
                                            activeOpacity={0.85}
                                            onPress={() => { navigation.navigate("ProductDetail", { data: item }) }}
                                        >
                                        </TouchableOpacity>
                                        <View style={styles.qntView}>

                                            <Text style={styles.price}>{'$ ' + item.price}</Text>
                                            <TouchableOpacity
                                                style={{ paddingLeft: 10, textAlign: "center" }}
                                                onPress={() => {
                                                    if (item.qty > 1) {
                                                        dispatch(reduceItemFromCart(item))
                                                    } else {
                                                        dispatch(removeItemFromCart(index))
                                                    }
                                                }}
                                            >
                                                <Text style={styles.btn}> - </Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={{ paddingLeft: 10, textAlign: "center" }}>
                                                <Text style={styles.btnQnt}> {item.qty} </Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity
                                                style={{ paddingLeft: 10, textAlign: "center" }}
                                                onPress={() => {
                                                    dispatch(addItemToCart(item))
                                                }}>
                                                <Text style={styles.btn}> + </Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>

                                </TouchableOpacity>
                            )
                        }}
                    />
                </View>
                <View style={styles.totalView}>
                    <Text style={styles.title}>Total: </Text>
                    <Text style={[styles.title, { marginRight: 20 }]}>${getTotal()}</Text>
                </View>
                <Text style={styles.title}>Select pyment method</Text>

                <View>
                    <TouchableOpacity activeOpacity={0.5} style={styles.paymentMethods}
                        onPress={() => { setselectedMethod(0) }}>
                        <Image
                            source={selectedMethod === 0
                                ? require('../images/radio_fill.png')
                                : require('../images/radio.png')
                            }
                            style={styles.img} />
                        <Text style={styles.paymentMethodsTxt}>Credit card</Text>
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={0.5} style={styles.paymentMethods}
                        onPress={() => { setselectedMethod(1) }}>
                        <Image
                            source={selectedMethod === 1
                                ? require('../images/radio_fill.png')
                                : require('../images/radio.png')
                            }
                            style={styles.img} />
                        <Text style={styles.paymentMethodsTxt}>Debit card</Text>
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={0.5} style={styles.paymentMethods}
                        onPress={() => { setselectedMethod(2) }}>
                        <Image
                            source={selectedMethod === 2
                                ? require('../images/radio_fill.png')
                                : require('../images/radio.png')
                            }
                            style={styles.img} />
                        <Text style={styles.paymentMethodsTxt}>UPI</Text>
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={0.5} style={styles.paymentMethods}
                        onPress={() => { setselectedMethod(3) }}>
                        <Image
                            source={selectedMethod === 3
                                ? require('../images/radio_fill.png')
                                : require('../images/radio.png')
                            }
                            style={styles.img} />
                        <Text style={styles.paymentMethodsTxt}>Cash on delivery</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.addressView}>
                    <Text style={styles.title}>Address</Text>
                    <Text
                        style={[styles.title, { textDecorationLine: 'underline', color: '#0087da' }]}
                        onPress={() => { navigation.navigate('Addresses') }}
                    >Edit address</Text>
                </View>
                <Text style={[styles.title, { marginTop: 10, fontSize: 16, color: "#636363" }]}>
                    {selectedAddress}
                </Text>
                <CustomButton
                    bg={"#008000"}
                    title={"Place order"}
                    width={"90%"}
                    onClick={() => {
                        console.log("order placed");
                    }}
                    txtcolor={"#fff"}
                    icon={require('../images/buy.png')}
                />
            </View>
        </ScrollView>
    )
}

export default Checkout

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        borderWidth: 1,
        borderColor: "red"
    },
    title: {
        fontSize: 20,
        fontWeight: "700",
        marginTop: 20,
        marginLeft: 20,
        color: '#000'
    },
    productItem: {
        width: Dimensions.get('window').width,
        height: 100,
        marginVertical: 5,
        backgroundColor: '#FFF',
        alignItems: "center",
        flexDirection: "row",
    },
    itemImage: {
        width: 100,
        height: 100,
        objectFit: "contain",
        marginLeft: 10,
    },
    name: {
        fontSize: 18,
        fontWeight: "600",
        marginLeft: 20,
        color: "#313232",
    },
    desc: {
        marginLeft: 20,
        color: "#3e3e3e",
    },
    link: {
        textDecorationLine: "underline",
        color: "#0087da",
        marginLeft: 20,
    },
    price: {
        color: "green",
        fontSize: 18,
        fontWeight: "600",
        marginLeft: 20,
    },
    qntView: {
        flexDirection: "row",
        paddingTop: 10,
    },
    btn: {
        width: 45,
        fontSize: 18,
        borderWidth: 0.2,
        borderRadius: 100,
        color: "#000",
        textAlign: "center",
    },
    btnQnt: {
        color: "#000",
        fontSize: 18,
    },
    totalView: {
        width: "100%",
        justifyContent: "space-between",
        marginTop: 50,
        flexDirection: "row",
        height: 70,
        alignItems: "center",
        borderBottomWidth: 0.25,
        borderBottomColor: "#b7b7b7",
    },
    paymentMethods: {
        flexDirection: "row",
        alignItems: "center",
        width: "90%",
        marginTop: 20,
        paddingLeft: 20,
    },
    img: {
        width: 24,
        height: 24
    },
    paymentMethodsTxt: {
        marginLeft: 15,
        fontSize: 16,
        color: "#000"
    },
    addressView: {
        width: "100%",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingRight: 20,
    }
})