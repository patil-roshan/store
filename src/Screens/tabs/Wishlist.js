import { Dimensions, FlatList, StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native'
import React, { useState } from 'react'
import { useSelector } from 'react-redux'
import Header from '../../common/Header';
import { useNavigation } from '@react-navigation/native';

const Wishlist = () => {
    const items = useSelector(state => state.wishlist);
    const navigation = useNavigation();
    const [wishlistItems, setWishlistItems] = useState(items.data);
    // console.log(JSON.stringify(items), items.data.length);

    return (
        <View style={styles.container}>
            <Header
                title={"Wishlist"}
                rightIcon={require('../../images/cart.png')}
                leftIcon={require('../../images/back.png')}
                onClickLeftIcon={() => {
                    navigation.replace("Main")
                }}
                onClickRightIcon={() => {
                    navigation.navigate("Cart");
                }}
            />
            <FlatList
                data={wishlistItems}
                renderItem={({ item, index }) => {
                    return (
                        <TouchableOpacity
                            activeOpacity={0.85}
                            style={styles.productItem}
                            onPress={
                                () => {
                                    navigation.navigate("ProductDetail", { data: item })
                                    // console.log("item:", item.id)
                                }
                            }
                        >
                            <Image
                                source={{ uri: item.image }}
                                style={styles.itemImage} />
                            <View>
                                <Text style={styles.name}>
                                    {item.title.length > 30
                                        ? item.title.substring(0, 30) + '..'
                                        : item.title}
                                </Text>
                                <Text style={styles.desc}>
                                    {item.description.length > 40
                                        ? item.description.substring(0, 40) + '...'
                                        : item.description}
                                </Text>
                                <TouchableOpacity
                                    activeOpacity={0.85}
                                    onPress={() => { navigation.navigate("ProductDetail", { data: item }) }}
                                >
                                </TouchableOpacity>
                                {/* <Text style={styles.price}>{'$ ' + item.price}</Text> */}
                            </View>

                        </TouchableOpacity>
                    )
                }}
            />
        </View>
    )
}

export default Wishlist

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    text: {
        color: "#000",
    },
    productItem: {
        width: Dimensions.get('window').width,
        height: 100,
        marginVertical: 5,
        backgroundColor: '#FFF',
        alignItems: "center",
        flexDirection: "row",
    },
    itemImage: {
        width: 100,
        height: 100,
        objectFit: "contain",
        marginLeft: 10,
    },
    name: {
        fontSize: 18,
        fontWeight: "600",
        marginLeft: 20,
        color: "#313232",
    },
    desc: {
        marginLeft: 20,
        color: "#3e3e3e",
    },
    link: {
        textDecorationLine: "underline",
        color: "#0087da",
        marginLeft: 20,
    },
    price: {
        color: "green",
        fontSize: 18,
        fontWeight: "600",
        marginLeft: 20,
    }
})