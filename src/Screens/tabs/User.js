import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import Header from '../../common/Header'
import { useNavigation } from '@react-navigation/native';

const User = () => {
    const navigation = useNavigation();
    return (
        <View style={styles.container}>
            <Header
                leftIcon={require('../../images/back.png')}
                title={"User"}
                rightIcon={require('../../images/cart.png')}
                onClickLeftIcon={() => {
                    navigation.replace('Main');
                }}
                onClickRightIcon={() => {
                    navigation.navigate('Cart');
                }}
            />
            <Image
                source={require('../../images/default_user.png')}
                style={styles.user} />
            <Text style={styles.name}>{"Roshan"}</Text>
            <Text style={[styles.name, { fontSize: 16 }]}>{"tmp@tmp.com"}</Text>

            <TouchableOpacity style={[styles.tab, { marginTop: 100 }]}>
                <Text style={{ color: '#000' }}>Edit profile</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.tab]}>
                <Text style={{ color: '#000' }}>Order</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.tab]}>
                <Text style={{ color: '#000' }}>Address</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.tab]}>
                <Text style={{ color: '#000' }}>Payment methods</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.tab]}>
                <Text style={{ color: '#000' }}>Logout    </Text>
            </TouchableOpacity>
        </View>
    )
}

export default User

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    user: {
        width: 100,
        height: 100,
        alignSelf: "center",
        marginTop: 100,
    },
    name: {
        textAlign: "center",
        fontSize: 20,
        color: "#000"
    },
    tab: {
        width: "90%",
        height: 50,
        borderBottomWidth: 0.25,
        alignSelf: "center",
        borderBottomColor: "#000",
        paddingLeft: 20,
        justifyContent: "center"
    }
})