import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Header from '../../common/Header'
import { useNavigation } from '@react-navigation/native'

const Notification = () => {
    const navigation = useNavigation()
    return (
        <View style={styles.container}>
            <Header
                leftIcon={require('../../images/menu.png')}
                title="Notification"
                rightIcon={require('../../images/cart.png')}
                onClickLeftIcon={() => {
                    navigation.replace('Main')
                }}
                onClickRightIcon={() => {
                    navigation.navigate('Cart')
                }}
            />
            <Text>Notification</Text>
        </View>
    )
}

export default Notification

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})