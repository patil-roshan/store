import { Dimensions, FlatList, Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import Header from '../../common/Header'
import { useNavigation } from '@react-navigation/native'
import { useDispatch } from 'react-redux'
import { addProducts } from '../../redux/slices/ProductsSlice'

const Home = () => {
    const navigation = useNavigation();
    const [products, setProducts] = useState({})
    const dispatch = useDispatch();
    useEffect(() => {
        getProducts()
    }, [])

    // console.log(products[0]);
    const getProducts = () => {
        fetch('https://dummyjson.com/products')
            .then(res => res.json())
            .then(json => {
                setProducts(json.products)
                json.products.map(item => {
                    item.qty = 1
                });
                dispatch(addProducts(json))
            })
    }

    // console.log(products[0]);
    return (
        <View style={styles.container}>
            <Header
                leftIcon={require('../../images/menu.png')}
                title={"Roshan Patil"}
                rightIcon={require('../../images/cart.png')}
                onClickLeftIcon={
                    () => {
                        navigation.openDrawer()
                    }
                }

            />
            <View style={{
                flex: 1,
                margin: 10
            }}>
                <FlatList
                    data={products}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item, index }) => {
                        return (
                            <TouchableOpacity
                                activeOpacity={0.85}
                                style={styles.productItem}
                                onPress={
                                    () => {
                                        navigation.navigate("ProductDetail", { data: item })
                                        // console.log("item:", item.id)
                                    }
                                }
                            >
                                <Image
                                    source={{ uri: item.thumbnail }}
                                    style={styles.itemImage} />
                                <View>
                                    <Text style={styles.name}>
                                        {item.title}
                                    </Text>
                                    <Text style={styles.desc}>
                                        {Dimensions.get('window').width < 400 ? item.description.substring(0, 30) + '...' : Dimensions.get('window').width < 450 ? item.description.substring(0, 35) + '...' : item.description}
                                        {/* {item.description} */}
                                        {/* {item.description.length > 40
                                            ? item.description.substring(0, 40) + '...'
                                            : item.description} */}
                                    </Text>
                                    <TouchableOpacity
                                        activeOpacity={0.85}
                                        onPress={() => { navigation.navigate("ProductDetail", { data: item }) }}
                                    >
                                    </TouchableOpacity>
                                    {/* <Text style={styles.price}>{'$ ' + item.price}</Text> */}
                                </View>

                            </TouchableOpacity>
                        )
                    }}
                />

            </View>
        </View >
    )
}

export default Home

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    productItem: {
        width: Dimensions.get('window').width,
        height: 100,
        marginVertical: 5,
        backgroundColor: '#FFF',
        alignItems: "center",
        flexDirection: "row",
    },
    itemImage: {
        width: 100,
        height: 100,
        objectFit: "contain",
        marginLeft: 10,
    },
    name: {
        fontSize: 18,
        fontWeight: "600",
        marginLeft: 20,
        color: "#313232",
    },
    desc: {
        marginLeft: 20,
        color: "#3e3e3e",
    },
    link: {
        textDecorationLine: "underline",
        color: "#0087da",
        marginLeft: 20,
    },
    price: {
        color: "green",
        fontSize: 18,
        fontWeight: "600",
        marginLeft: 20,
    }
})