import { Dimensions, FlatList, Image, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import React, { useState } from 'react'
import { useSelector } from 'react-redux'
import Header from '../../common/Header';
import { useNavigation, useRoute } from '@react-navigation/native';

const Search = () => {
    const products = useSelector(state => state);
    const [search, setSearch] = useState("");
    const [oldData, setoldData] = useState(products.product.data);
    const [searchedList, setSearchedList] = useState([]);
    const navigation = useNavigation();
    // console.log(navigation.getState().routes);
    // console.log(JSON.stringify(products.product.data));
    const filterData = (txt) => {
        let newData = oldData.filter(item => {
            return item.title.toLowerCase().match(txt.toLowerCase())
        });
        setSearchedList(newData);
    }

    // const navigation = useNavigation()
    const route = useRoute()
    return (
        <View style={styles.container}>
            <Header
                leftIcon={require('../../images/back.png')}
                title={"Search"}
                rightIcon={require('../../images/cart.png')}
                onClickLeftIcon={
                    () => {
                        navigation.navigate("Main");
                    }
                }
                onClickRightIcon={
                    () => {
                        navigation.navigate("Cart")
                    }
                }
            />
            <View style={styles.searchView}>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Image source={require('../../images/search.png')} style={styles.searchViewIcon} />
                    <TextInput
                        style={styles.input}
                        placeholder='Search product'
                        placeholderTextColor={"#afafaf"}
                        value={search}
                        onChangeText={(txt) => {
                            setSearch(txt)
                            filterData(txt)
                        }}

                    />
                </View>
                {search !== "" && (
                    < TouchableOpacity
                        onPress={() => {
                            setSearch("")
                            filterData("")
                        }}
                    >
                        <Image source={require('../../images/close.png')} style={styles.searchViewIcon} />
                    </TouchableOpacity>)}
            </View>
            <FlatList
                data={searchedList}
                renderItem={({ item, index }) => {
                    return (
                        <TouchableOpacity
                            activeOpacity={0.85}
                            style={styles.productItem}
                            onPress={
                                () => {
                                    navigation.navigate("ProductDetail", { data: item })
                                    // console.log("item:", item.id)
                                }
                            }
                        >
                            <Image
                                source={{ uri: item.image }}
                                style={styles.itemImage} />
                            <View>
                                <Text style={styles.name}>
                                    {item.title.length > 30
                                        ? item.title.substring(0, 30) + '..'
                                        : item.title}
                                </Text>
                                <Text style={styles.desc}>
                                    {item.description.length > 40
                                        ? item.description.substring(0, 40) + '...'
                                        : item.description}
                                </Text>
                                <TouchableOpacity
                                    activeOpacity={0.85}
                                    onPress={() => { navigation.navigate("ProductDetail", { data: item }) }}
                                >
                                </TouchableOpacity>
                                {/* <Text style={styles.price}>{'$ ' + item.price}</Text> */}
                            </View>

                        </TouchableOpacity>
                    )
                }}
            />
        </View >
    )
}

export default Search

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
    },
    text: {
        color: "#000",
    },
    searchView: {
        width: "90%",
        height: 40,
        borderRadius: 10,
        borderWidth: 0.5,
        alignSelf: "center",
        alignItems: "center",
        justifyContent: "space-between",
        marginVertical: 25,
        flexDirection: "row",
        paddingHorizontal: 15
    },
    searchViewIcon: {
        width: 20,
        height: 20,
        resizeMode: "center",
    },
    input: {
        width: "90%",
        paddingLeft: 10,
        color: "#000",
    },
    productItem: {
        width: Dimensions.get('window').width,
        height: 100,
        marginVertical: 5,
        backgroundColor: '#FFF',
        alignItems: "center",
        flexDirection: "row",
    },
    itemImage: {
        width: 100,
        height: 100,
        objectFit: "contain",
        marginLeft: 10,
    },
    name: {
        fontSize: 18,
        fontWeight: "600",
        marginLeft: 20,
        color: "#313232",
    },
    desc: {
        marginLeft: 20,
        color: "#3e3e3e",
    },
    link: {
        textDecorationLine: "underline",
        color: "#0087da",
        marginLeft: 20,
    },
    price: {
        color: "green",
        fontSize: 18,
        fontWeight: "600",
        marginLeft: 20,
    }
})