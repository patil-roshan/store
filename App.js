import { StyleSheet, Text, View, LogBox } from 'react-native'
import React, { useEffect } from 'react'
import AppNavigator from './src/AppNavigator';
import { Provider } from 'react-redux';
import { store } from './src/redux/store';

const App = () => {

  useEffect(() => {
    LogBox.ignoreLogs(['VirtualizedLists should never be', 'Selector unknown', "ReactImageView"]);
  }, [])
  console.log("App started...");
  return (
    <Provider store={store}>
      <AppNavigator />
    </Provider>
  )
}

export default App

const styles = StyleSheet.create({})